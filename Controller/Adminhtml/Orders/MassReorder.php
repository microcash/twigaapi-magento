<?php

namespace Microcash\Twiga\Controller\Adminhtml\Orders;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\App\ResourceConnection;
use Microcash\Twiga\Api\MicroCashApi;

class MassReorder extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{

    protected $orderManagement;
    protected $resultRedirectFactory;
    protected $_resource;
    protected $_orderHandler;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ResourceConnection $resource,
        OrderManagementInterface $orderManagement,
        \Microcash\Twiga\Model\Orders $orderHandler,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        parent::__construct($context, $filter);
        $this->_orderHandler = $orderHandler;
        $this->_resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->orderManagement = $orderManagement;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    protected function massAction(AbstractCollection $collection)
    {
        $sendAttempts = 0;
        $tableName = $this->_resource->getTableName('sales_order');

        $succesfullySent = 0;

        foreach ($collection->getItems() as $order) {
            if (!$order->getEntityId()) {
                continue;
            }

            
            
            if($order->getData('microcash_processed') == MicroCashApi::MC_ORDER_PROCESS_STATUS_FAILED) {
                
                $sql='UPDATE '.$tableName.' SET status = \''.\addslashes('pending').'\', microcash_processed = null, microcash_processed_date = null where entity_id='.$order->getEntityId();
                $connection = $this->_resource->getConnection();
                $connection->query($sql);

                $this->_orderHandler->updateOrders();

                $sendAttempts++;

                if($order->getData('microcash_processed_errordata') == null) {
                    
                    $succesfullySent += 1;
                }            
            }
        } 

        $alreadySent = $collection->count() - $sendAttempts; // this means it was selected in the UI, but status was not 2.
        $failedAttempts = $sendAttempts - $succesfullySent;

        if ($alreadySent && $sendAttempts) {
            $this->messageManager->addError(__('%1 order(s) failed to resent.', $failedAttempts));
            $this->messageManager->addError(__('%1 order(s) have already been sent', $alreadySent));
        } elseif ($alreadySent) {
            $this->messageManager->addError(__('All selected orders have already been sent. Only failed orders can be sent again.'));
        }

        if ($succesfullySent) {
            $this->messageManager->addSuccess(__('%1 order(s) have been successfully sent to Twiga.', $succesfullySent));
            
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}