<?php

namespace Microcash\Twiga\Controller\Adminhtml\Orders;

class Index extends \Magento\Backend\App\Action {

    protected $_logger;
    protected $_ordersModel;

    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
            \Psr\Log\LoggerInterface $logger, 
            \Microcash\Twiga\Model\Orders $ordersModel
    ) {
        parent::__construct($context);
        $this->_logger = $logger;
        $this->_ordersModel = $ordersModel;
    }

    public function execute($page = false) {
        echo 'Transmitting all untransmitted orders to Twiga...<br>';
        $this->_ordersModel->updateOrders($debug=true);
    }

    function test()
    {
        echo 'TEST';
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Microcash_Twiga::orders');
    }

}
