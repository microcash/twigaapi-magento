<?php

namespace Microcash\Twiga\Controller\Adminhtml\Syncit;

use Magento\Framework\App\Action\Context;
use Microcash\Twiga\Api\MicroCashApi;
use Microcash\Twiga\Model\Stock;

class Index extends \Magento\Backend\App\Action {

    protected $_logger;
    protected $_productCollection;
    protected $_productRepository;
    protected $_stockRegistry;
    protected $_cronModel;

    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
            \Psr\Log\LoggerInterface $logger, 
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection, 
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, 
            \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, 
            Stock $cronModel
    ) {
		
        parent::__construct($context);
        $this->_logger = $logger;
        $this->_productCollection = $collection;
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_cronModel = $cronModel;
    }


    public function execute($page = false) {
        $this->_cronModel->updateAllStock(0, true);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Microcash_Twiga::manage');
    }

}
