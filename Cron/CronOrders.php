<?php

namespace Microcash\Twiga\Cron;


use Magento\Backend\App\Action\Context;
use Microcash\Twiga\Model\Stock;

class CronOrders {

    protected $_modelStock;
    protected $_modelOrders;
    protected $_logger;

    public function __construct(
		Context $context, 
        \Microcash\Twiga\Model\Orders $modelOrders,
        \Psr\Log\LoggerInterface $logger,
        Stock $modelStock
    ) {
        $this->_modelOrders = $modelOrders;
        $this->_modelStock = $modelStock;
        $this->_logger = $logger;
    }

    public function execute() {
        try
        {
        $this->_modelOrders->updateOrders();
        $this->_modelStock->updateAllStock(0);
        } catch (\Throwable $ex)
        {
            $this->_logger->warn(__METHOD__ . 'CRON DING #################################### : ' . $ex->getMessage(). date('d-m-Y H:i:s', strtotime('+1 hours')));
        }
    }

   
}
