<?php

namespace Microcash\Twiga\Cron;

use Psr\Log\LoggerInterface;
use Magento\Backend\App\Action\Context;
use Microcash\Twiga\Model\Stock;

class CronStock {

    protected $_mcApi;
    protected $_modelStock;

    public function __construct(
		Context $context, 
		LoggerInterface $logger, 
        \Microcash\Twiga\Api\MicroCashApi $mcApi,
        Stock $modelStock
    ) {
        $this->_mcApi = $mcApi;
        $this->_modelStock = $modelStock;
    }

    public function execute() {
        $this->_modelStock->updateAllStock(0);
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Microcash_Twiga::manage');
    }

}
