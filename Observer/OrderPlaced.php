<?php

namespace Microcash\Twiga\Observer;

use Magento\Framework\Event\ObserverInterface;

class OrderPlaced implements ObserverInterface{

    protected $_mcApi;
    protected $_orders;

    public function __construct(
            \Microcash\Twiga\Model\Orders $orders
    ) {
        $this->_orders = $orders;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_orders->updateOrders();
    }

}
