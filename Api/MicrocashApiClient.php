<?php
	
    namespace Microcash\Twiga\Api;

    use SoapClient;
    
    ini_set('max_input_time', -1);
    ini_set("default_socket_timeout", 10);
    ini_set("soap.wsdl_cache_enabled", 0);

    error_reporting(E_ALL);

    class MicrocashApiClient {

        protected $_scopeConfig;

        protected $_username;
        protected $_filiaal; 
        protected $_key;
        protected $_soapUrl; 
        protected $_wsdlUrl;

        public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
            $this->_scopeConfig = $scopeConfig;
            $this->_username = $this->_scopeConfig->getValue('Microcash-settings/soap_client/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $this->_filiaal = $this->_scopeConfig->getValue('Microcash-settings/soap_client/filiaal', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);           
            $this->_key = $this->_scopeConfig->getValue('Microcash-settings/soap_client/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $this->_soapUrl = $this->_scopeConfig->getValue('Microcash-settings/soap_client/soap_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $this->_wsdlUrl = $this->_scopeConfig->getValue('Microcash-settings/soap_client/wsdl_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }



        public $_also_sync_pending_orders = true; 

        function DeriveHash($username, $secretkey, $requestid, $lang = 1)
        {
            $data = utf8_encode(strtoupper($username.$secretkey.strval($lang).$requestid));
            $hash = base64_encode(hash('sha256', $data, true ));
            return $hash;
        }

        function FiliaalID()
        {
            return $this->_filiaal;
        }
    
        function GUID()
        {
            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);
            $uuid = chr(123)
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);
            return trim($uuid, '{}');
        }
    
        function BouwHeader()
        {
            $request = $this->GUID();
            $header = [];
            $header['Language'] = 1;
            $header['Username'] = $this->_username;
            $header['Filiaal'] = $this->_filiaal;
            $header['RequestID'] = $request;
            $header['Hash'] = $this->DeriveHash($this->_username, $this->_key, $request, '1');
            return $header;
        }
    
        function BuildSoapClient()
        {
            $options = array(
                'location' => $this->_soapUrl,
                'uri' => $this->_soapUrl,
                'trace' => true,
                'keep_alive' => false,
                'connection_timeout' => 10,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'features'=>SOAP_SINGLE_ELEMENT_ARRAYS,
                'exceptions' 
                => true,
            );
            return new SoapClient($this->_wsdlUrl, $options);
        }

    }    