<?php
	
namespace Microcash\Twiga\Api;

ini_set('max_input_time', -1);
ini_set("default_socket_timeout", 10);
ini_set("soap.wsdl_cache_enabled", 0);

error_reporting(E_ALL);



class MicroCashApi {

    const MC_ORDER_PROCESS_STATUS_PENDING = 0;
    const MC_ORDER_PROCESS_STATUS_SUCCESS = 1;
    const MC_ORDER_PROCESS_STATUS_FAILED = 2;

    protected $soapclient;
    protected $_logger;
    protected $_resource;
    protected $_mcApiClient;


    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\ResourceConnection $resource,
        \Microcash\Twiga\Api\MicrocashApiClient $apiClient
        ) 
    {
        $this->_logger = $logger;
        $this->_resource = $resource;
        $this->_mcApiClient = $apiClient;
    }

    function GetProductStock($barcode)
    {
        $productDetailsRequest = [];
        $productDetailsRequest['IdentificationMethod'] = "Barcode";
        $productDetailsRequest['IdentificatieKey'] = $barcode;
        $productDetailsRequest['DetailFields'] = "InStock";

        $params = [];
        $params['pHeader'] = $this->_mcApiClient->BouwHeader();
        $params['pProductDetailsRequest'] = $productDetailsRequest;

        $soapClient = $this->_mcApiClient->BuildSoapClient();
        $response = $soapClient->GetProductDetails($params);
        $detailsArray = $response->GetProductDetailsResult->LocationDetails->ProductLocationDetails;

        foreach($detailsArray as $voorraad)
        {
            if($voorraad->StoreId == $this->_mcApiClient->FiliaalID())
            {
                return $voorraad->InStock;
            }
        }
    }

    public function DetermineIfOrderWasAlreadySent($orderID)
    {
        $params = [];
        $params['pHeader'] = $this->_mcApiClient->BouwHeader();
        $params['pReference'] = $orderID;

        $soapClient = $this->_mcApiClient->BuildSoapClient();
        $response = $soapClient->ReferenceAlreadyUsed($params);
        $data = $response->ReferenceAlreadyUsedResult;

        if ($data->Success)
        {
            return $data->ReferenceUsed;
        }
        return false;
    }

    public function GetStocks($barcodeArray) {
        return array_map(array($this, "GetProductStock"), $barcodeArray);
    }

    public function ConfirmOrder($order_info) {
        if (empty($order_info['orderId'])) {
            $this->_logger->warn(__METHOD__ . ' :: no orderId found');
            return false;
        }

        $tableName = $this->_resource->getTableName('sales_order');

        $orderData = array(
            "orderid" => $order_info['orderId'],
            "fullname" => $order_info['customer']['firstname'] . ' ' . $order_info['customer']['lastname'],
            "address" => $order_info['customer']['street'] . " ",
            "postalcode" => $order_info['customer']['postcode'],
            "city" => $order_info['customer']['city'],
            "email" => $order_info['customer']['email'],
            "note" => (isset($order_info["comment"])) ? $order_info["comment"] : '',
            "products" => $order_info['products']
        );


        if ($this->DetermineIfOrderWasAlreadySent($order_info['orderId']))
        {
            //The order has already been sent. Mark order as MC_ORDER_PROCESS_STATUS_SUCCES.
            $sql="UPDATE ".$tableName." SET  microcash_processed = ".self::MC_ORDER_PROCESS_STATUS_SUCCESS.", microcash_processed_date = NOW(), microcash_processed_errordata = null where entity_id=".$orderData['orderid'];
            $connection = $this->_resource->getConnection();
            $connection->query($sql);
            return true;
        }        
        

        $soapClient = null;

		try {

            $nameArray = explode(' ', $orderData['fullname'], 2);
            $salesarray = [];
            $totalprice = 0;
    
            $customerRecord = [];
            $customerRecord['Emails'] = [$orderData['email']];
            $customerRecord['Voornaam'] = $nameArray[0];
            $customerRecord['Achternaam'] = $nameArray[1];
    
            $customerData = [];
            $customerData['CustomerRecord'] = $customerRecord;
    
            foreach($orderData['products'] as $prod)
            {
                $orderLine = [];
                $orderLine['ProductID'] = [];
                $orderLine['ProductID']['Barcode'] = $prod['barcode'];
                $orderLine['Amount'] = $prod['quantity'];
                $orderLine['Price'] = $prod['price'];
                array_push($salesarray, $orderLine);
                $totalprice += ($prod['quantity'] * $prod['price']);
            }
    
            $order = [];
            $order['CountryCode'] = "NL";
            $order['DoNotChangeStock'] = false;
            $order['OrderDateUTC'] = date(DATE_ATOM);
            $order['OrderReference'] = $orderData['orderid'];
            $order['Payment'] = "Paid";
            $order['PaymentName'] = $order_info['paymenttype'];
            $order['Sales'] = $salesarray;


            $totalprice += $order_info['shipping'];
            $totalprice += $order_info['paymentfee'];

            $order['ShippingCosts'] = $order_info['shipping'];
            $order['PaymentMethodCosts'] = $order_info['paymentfee'];

            $order['TotalAmount'] = $totalprice;


            //print_r($order); //debug
    
            $submitOrderRequest = [];
            $submitOrderRequest['CustomerInfo'] = $customerData;
            $submitOrderRequest['Order'] = $order;
    
            $params = [];
            $params['pHeader'] = $this->_mcApiClient->BouwHeader();
            $params['pRequest'] = $submitOrderRequest;
    
            $soapClient = $this->_mcApiClient->BuildSoapClient();
            $response = $soapClient->AddOrder($params);
    
            $detailsArray = $response->AddOrderResult;
            
            if($detailsArray->Success) {
                $sql="UPDATE ".$tableName." SET  microcash_processed = ".self::MC_ORDER_PROCESS_STATUS_SUCCESS.", microcash_processed_date = NOW(), microcash_processed_errordata = null where entity_id=".$orderData['orderid'];
                $connection = $this->_resource->getConnection();
                $connection->query($sql);
                return true;
            } else {
                $sql='UPDATE '.$tableName.' SET microcash_processed = '.self::MC_ORDER_PROCESS_STATUS_FAILED.', microcash_processed_date = NOW(), microcash_processed_errordata = \''.\addslashes(json_encode($detailsArray)).'\' where entity_id='.$orderData['orderid'];
                $connection = $this->_resource->getConnection();
                $connection->query($sql);

                $debug_info = '';
                $this->_logger->warn(__METHOD__ . ' :: no success DEBUG INFO:'.$debug_info.' '. date('d-m-Y H:i:s', strtotime('+1 hours')).' -> DETAILS:'.json_encode($detailsArray));
                return false;
            }
        } catch (\Throwable  $ex) {
            $sql2 = '';
            try
            {
                $errorasText = json_encode($ex);

                $errorasText .= "REQUEST:\r\n";
                if ($soapClient) {
                    $errorasText .= json_encode($soapClient->__getLastRequest());
                }

                $sql2='UPDATE '.$tableName.' SET microcash_processed = '.self::MC_ORDER_PROCESS_STATUS_FAILED.', microcash_processed_date = NOW(), microcash_processed_errordata = \''.\addslashes($errorasText).'\' where entity_id='.$orderData['orderid'];
                $connection = $this->_resource->getConnection();
                $connection->query($sql2);
            }
            catch (\Throwable  $ex2)
            {
                $this->_logger->warn(__METHOD__ . 'Exception in marking order as fubar: ' . $ex2->getMessage(). date('d-m-Y H:i:s', strtotime('+1 hours')).$sql2 );    
            }
            $this->_logger->warn(__METHOD__ . 'Exception: ' . $ex->getMessage(). date('d-m-Y H:i:s', strtotime('+1 hours')));
            return false;
        }
    }
   
    public function GetStocksSince($version)
    {
        $changedStocksRequest = [];
        $changedStocksRequest['ChangedSinceTimestamp'] = $version;
        $changedStocksRequest['MaxResults'] = 20000;

        $params = [];
        $params['pHeader'] = $this->_mcApiClient->BouwHeader();
        $params['pRequest'] = $changedStocksRequest;

        $result = $this->_mcApiClient->BuildSoapClient()->GetChangedStocks($params);
        return $result;
    }
}
