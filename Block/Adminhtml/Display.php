<?php

namespace Microcash\Twiga\Block\Adminhtml;

class Display extends \Magento\Framework\View\Element\Template {

    public function __construct(\Magento\Framework\View\Element\Template\Context $context) {
        parent::__construct($context);
    }

    public function sayHello() {
        echo __METHOD__ . '<br>';
        return __('Hello World');
    }
    
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Microcash_Twiga::manage');
    }

}
