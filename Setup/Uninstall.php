<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Microcash\Twiga\Setup;

 
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
 
class Uninstall implements UninstallInterface
{
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->dropColumn($setup->getTable('sales_order'), 'microcash_processed');
        $setup->getConnection()->dropColumn($setup->getTable('sales_order'), 'microcash_processed_date');
        $setup->endSetup();
    }
}