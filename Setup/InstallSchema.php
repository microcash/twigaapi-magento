<?php

namespace Microcash\Twiga\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $setup->startSetup();

        $connection = $setup->getConnection();

        $column = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            'unsigned' => true,
            'nullable' => true,
            'default' => NULL,
            'comment' => 'processed date'
        ];
        $connection->addColumn($setup->getTable('sales_order'), 'microcash_processed_date', $column);

        $column = [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            'length' => 11,
            'nullable' => true,
            'default' => null,
            'comment' => 'processed state, NULL = not processed, 1 = processed, 2 = errored'
        ];
        $connection->addColumn($setup->getTable('sales_order'), 'microcash_processed', $column);

        $setup->endSetup();
    }

}
