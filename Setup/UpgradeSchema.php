<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Microcash\Twiga\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use mysql_xdevapi\Exception;

class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        
        $setup->startSetup();

        if (!$context->getVersion()) {
            //no previous version found, installation, InstallSchema was just executed
            //be careful, since everything below is true for installation !
        }

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $table = $setup->getConnection()
            ->newTable($setup->getTable('microcash_sync_status'))
            ->addColumn(
                'mc_sync_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Synctype ID'
            )
            ->addColumn(
                'sync_type',
                \Magento\Framework\DB\Ddl\TABLE::TYPE_TEXT,
                32,
                ['nullable' => false],
                'The type of the syncfunction'
            )
            ->addColumn(
                'sync_state',
                \Magento\Framework\DB\Ddl\TABLE::TYPE_BIGINT,
                null,
                ['nullable' => false],
                'The state of the sync counter'
            );
            $setup->getConnection()->createTable($table);
        }


        if (version_compare($context->getVersion(), '1.0.2') < 0) {

            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'microcash_processed_errordata',
                [
                    'length' => '2M',
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => NULL,
                    'comment' => 'Possible sync error'
                ]);
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_grid'),
                'microcash_processed_errordata',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => '2M',
                    'nullable' => true,
                    'default' => NULL,
                    'comment' => 'Possible sync error'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_grid'),
                'microcash_processed',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'processed state, NULL = not processed, 1 = processed, 2 = errored'
                ]
            );
        }
        $setup->endSetup();
    }






}
