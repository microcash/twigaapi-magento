<?php

namespace Microcash\Twiga\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Encryption\Encryptor;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetup;
use Magento\Sales\Setup\SalesSetupFactory;
use Microcash\Twiga\Api\MicroCashApi;

use const Microcash\Twiga\Api\MC_ORDER_PROCESS_STATUS_SUCCESS;

class UpgradeData implements UpgradeDataInterface {

    private $eavSetupFactory;
    protected $categorySetupFactory;
    protected $quoteSetupFactory;
    protected $salesSetupFactory;


    public function __construct(EavSetupFactory $eavSetupFactory, SalesSetupFactory $salesSetupFactory) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $data = [
                [
                    'sync_type' => 'Products',
                    'sync_state' => 0,
                ],
                [
                    'sync_type' => 'Stock',
                    'sync_state' => 0,
                ],
                [
                    'sync_type' => 'Relations',
                    'sync_state' => 0,
                ],

            ];

            // Insert data to table
            foreach ($data as $item) {
                $setup->getConnection()->insert('microcash_sync_status', $item);
            }

            //init all existing orders and mark as processed to avoid uploading a full backlog
            $setup->getConnection()->exec('update '.$setup->getTable('sales_order').' set microcash_processed_date = \'1971-01-01\', microcash_processed='.MicroCashApi::MC_ORDER_PROCESS_STATUS_SUCCESS);
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {

        }

        $setup->endSetup();
    }

}

