<?php

namespace Microcash\Twiga\Model;

use Psr\Log\LoggerInterface;
use Magento\Backend\App\Action\Context;

class Stock {

    protected $_logger;
    protected $_productCollection;
    protected $_productRepository;
    protected $_stockRegistry;
    protected $_orderRepository;
    protected $_customer;
    protected $catalogSession;
    protected $_resource;
    protected $_mcApi;

    public function __construct(
		Context $context, 
		LoggerInterface $logger, 
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection, 
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository, 
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, 
		\Magento\Sales\Model\OrderRepository $orderRepository, 
		\Magento\Customer\Model\Customer $customer, 
		\Magento\Framework\App\ResourceConnection $resource,
        \Microcash\Twiga\Api\MicroCashApi $mcApi
    ) {

        $this->_logger = $logger;
        $this->_productCollection = $collection;
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_orderRepository = $orderRepository;
        $this->_customer = $customer;
        $this->_resource = $resource;
        $this->_mcApi = $mcApi;
    }

    public function execute() {
        $this->updateAllStock(0);
    }

    public function updateAllStock($page, $debug = false) {
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getTableName('microcash_sync_status');
        $sql = 'SELECT sync_state FROM '.$tableName.' WHERE sync_type="Stock"';


        $tableCatalogStock = $this->_resource->getTableName('cataloginventory_stock_item');
        $tableCatalogStockStatus = $this->_resource->getTableName('cataloginventory_stock_status');
        $tableCatalogEntity = $this->_resource->getTableName('catalog_product_entity');
        $tableInventoryReservations = $this->_resource->getTableName('inventory_reservation');

        $inventory_reservatation_enabled = $connection->fetchRow('SELECT COUNT(*) C FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=\''.$tableInventoryReservations.'\'')['C'] > 0;

       
        $resultcount = 0;
        do
        {
            $sync_offset = $connection->fetchRow($sql)['sync_state'];
            $stocks_since = $this->_mcApi->GetStocksSince($sync_offset);
            $res = $stocks_since->GetChangedStocksResult;
            echo("<script>console.log('1');</script>");
            echo("<script>console.log('PHP: " . $sync_offset. "');</script>");
            echo("<script>console.log('PHP: " . $res->MaxChangeTimestamp. "');</script>");



            if ($sync_offset != $res->MaxChangeTimestamp)
            {
                echo("<script>console.log('2');</script>");
                //print_r($res);
                $resultcount = count($res->CurrentStock->CurrentStock);
                $count = 0;

                $updateQuery = '';
                $connection->beginTransaction();
                foreach ($res->CurrentStock->CurrentStock as $item)
                {
                    echo("<script>console.log('3');</script>");
                    if (isset($item->MainBarcode))
                    {
                        echo("<script>console.log('4');</script>");
  
                        $count++;
                        // $affected = $connection->exec('
                        // UPDATE '.$tableCatalogStock.' STOCK 
                        // INNER JOIN '.$tableCatalogEntity.' PROD on STOCK.product_id = PROD.entity_id 
                        // INNER JOIN '.$tableCatalogStockStatus.' STOCKSTAT on STOCKSTAT.product_id = PROD.entity_id
                        // SET 
                        //     STOCK.Qty='.addslashes($item->StockValue).', 
                        //     STOCK.is_in_stock='.($item->StockValue > 0 ? 1 : 0).',
                        //     STOCKSTAT.Qty='.addslashes($item->StockValue).',
                        //     STOCKSTAT.stock_status='. ($item->StockValue > 0 ? 1 : 0).'
                        // WHERE PROD.sku=\''.addslashes($item->MainBarcode) .'\'');

                        $product = null;
                        try
                        {
                            $product = $this->_productRepository->get($item->MainBarcode);
                            echo("<script>console.log('5');</script>");
                        } catch (\Magento\Framework\Exception\NoSuchEntityException $e)
                        {
                            $this->_logger->warn('update stock '.$item->MainBarcode.' does not exist');
                            echo 'update stock '.$item->MainBarcode.' does not exist'.'<br />';
                            $product = false;
                        }

                        if ($product)
                        {
                            echo("<script>console.log('6');</script>");
                            $stockItem = $this->_stockRegistry->getStockItem($product->getId());
                            if ($stockItem->getQty() != $item->StockValue)
                            {
                                echo("<script>console.log('7');</script>");
                                $stockItem->setData('qty',  $item->StockValue); //set updated quantity
                                $stockItem->setData('is_in_stock', ($item->StockValue > 0) ? 1 : 0);

                                $affected = null;

                                try {
                                    echo("<script>console.log('8');</script>");
                                    $affected = $this->_stockRegistry->updateStockItemBySku($item->MainBarcode, $stockItem);
                                    $this->_logger->warn('update stock '.$item->MainBarcode.' successfully set to '.$item->StockValue);
                                    echo 'update stock '.$item->MainBarcode.' successfully set to '.$item->StockValue.'<br />';
                                    if ($inventory_reservatation_enabled && $affected)
                                    {
                                        $connection->query('DELETE FROM '.$tableInventoryReservations.' WHERE sku=\''.addslashes($item->MainBarcode) .'\'');
                                    } 
                                } catch (\Exception $ex) {
                                    $this->_logger->warn('MicroCashApi::updateProductStock :: Exception:' . $ex->getMessage(). date('d-m-Y H:i:s', strtotime('+1 hours')));
                                    echo $ex->getMessage();
                                    return false;
                                }
                            }
                        }
                    }
                }

                $connection->query('UPDATE '.$tableName.' SET sync_state='.$res->MaxChangeTimestamp.' WHERE sync_type="Stock"');
                $connection->commit();
                echo 'Updated until timestamp:'.$res->MaxChangeTimestamp;
                echo 'Update stock for '.$resultcount.' records...';
                break;
            } else {
                echo("<script>console.log('break');</script>");
                break; //no new results. So we're done.
            }
        } while ($resultcount <> 0);
        echo 'STOCKS ARE UP TO DATE WITH TWIGA';
    }

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Microcash_Twiga::manage');
    }

}
