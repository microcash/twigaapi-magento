<?php


namespace Microcash\Twiga\Model;



class Orders extends \Magento\Backend\App\Action
{

    protected $_logger;
    protected $_productCollection;
    protected $_productRepository;
    protected $_stockRegistry;
    protected $_customer;
    protected $catalogSession;
    protected $_salesOrderCollectionFactory;
    protected $_orderRepository;
    protected $_resource;
    protected $_mcApi;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\App\ResourceConnection $resource,
        \Microcash\Twiga\Api\MicroCashApi $twigaAPI
    )
    {
        $this->_logger = $logger;
        $this->_productCollection = $collection;
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_customer = $customer;
        $this->catalogSession = $catalogSession;
        $this->_salesOrderCollectionFactory = $salesOrderCollectionFactory;
        $this->_orderRepository = $orderRepository;
        $this->_resource = $resource;
        $this->_mcApi = $twigaAPI;
    }

    public function execute()
    {
        $this->updateOrders();
    }

    public function updateOrders($debug = false)
    {
        if ($debug)
        {
            echo 'Start debugging<br>';
            error_reporting(1);
            ini_set('display_errors', 1);
            echo 'start:' . date('H:i:s') . '<br>';
            echo '<pre>';
        }

        // Mark failed orders as pending in order to perform an automatic retry
        {
            $connection = $this->_resource->getConnection();
            $tableName = $this->_resource->getTableName('sales_order');
            //set a maximum of 5 previously failed orders which failed at least 30 minutes ago as pending.
            $sql = "UPDATE `".$tableName."` 
            SET microcash_processed_date=null, microcash_processed=null
            WHERE microcash_processed = 2 AND DATE_ADD(microcash_processed_date, INTERVAL 30 MINUTE) < NOW() LIMIT 5";
            $connection->query($sql);
        }

        // Clear error message for orders which are successfully sent
        {
            $connection = $this->_resource->getConnection();
            $tableName = $this->_resource->getTableName('sales_order');
            $sql = "UPDATE `".$tableName."` SET microcash_processed_errordata=null WHERE microcash_processed = 1";
            $connection->query($sql);
        }

        $orderCollection = $this->_salesOrderCollectionFactory->create();
        $orderCollection->addAttributeToSelect('*');
        $clone = clone $orderCollection;
        if ($debug)
        {
            echo 'aantal verwerkte orders: ' . $clone->getSize() . '<br>';
        }
        
        $orderCollection->addFieldToFilter('microcash_processed_date', ['null' => true]);

        //pending means: can still be cancelled.

        $also_sync_pending = true; //TODO: instellingen openemen.

        if ($also_sync_pending)
        {
            $orderCollection->addAttributeToFilter('status', array('in' => array('processing', 'in_progress', 'not_complete', 'complete', 'closed', 'pending')));
        }
        else
        {
            $orderCollection->addAttributeToFilter('status', array('in' => array('processing', 'in_progress', 'not_complete', 'complete', 'closed')));
        }



        if ($debug) {
            echo 'aantal onverwerkte orders: ' . $orderCollection->getSize() . '<br>';
        }

        if ($orderCollection->getSize() > 0) {
            foreach ($orderCollection as $orderData) {
                $orderId = $orderData->getId();
                $order = $this->_orderRepository->get($orderId);
                //$incrementId = $order->getIncrementId();
                $customer = $this->_customer->load($order->getCustomerId());
                $shippingAddress = $order->getShippingAddress()->getData();
                if ($debug)
                {
                    echo "orderid: " . $orderId . ", customerid: ".$order->getCustomerId().", klant: " . $customer->getFirstname() . " " . $customer->getLastname().'</br>';
                }
                $order_info = [];

                foreach ($order->getAllVisibleItems() as $k => $item) {

                    $order_info['products'][$k]['product_id'] = $item['product_id'];
                    $order_info['products'][$k]['barcode'] = ltrim($item['sku'], '0');
                    $order_info['products'][$k]['quantity'] = str_replace('.0000', '', $item['qty_ordered']);
                    $order_info['products'][$k]['price'] = money_format('%.2n', $item->getPriceInclTax());
                }

                $order_info['orderId'] = $orderId;
                $order_info['customer']['customerId'] = $order->getCustomerId();
                if ($order->getCustomerId()) {
                    $order_info['customer']['firstname'] = $customer->getFirstname();
                    $order_info['customer']['lastname'] = $customer->getLastname();
                } else {
                    $order_info['customer']['firstname'] = $shippingAddress['firstname'];
                    $order_info['customer']['lastname'] = $shippingAddress['lastname'];
                }
                $order_info['customer']['street'] = $shippingAddress['street'];
                $order_info['customer']['postcode'] = $shippingAddress['postcode'];
                $order_info['customer']['city'] = $shippingAddress['city'];
                $order_info['customer']['email'] = $shippingAddress['email'];

                $order_info['shipping'] = 0;
                $order_info['paymentfee'] = 0;
                $order_info['paymenttype'] = '';

                if ($order->getPayment()->getData() &&
                 $order->getPayment() && 
                 $order->getPayment()->getData()['additional_information'] && 
                 $order->getPayment()->getData()['additional_information']['method_title']) {
                    $order_info['paymenttype'] = $order->getPayment()->getData()['additional_information']['method_title'];
                    
                }

                if ($order->getShippingMethod()) {
                    print_r($order->getShippingMethod());
                    $order_info['shipping'] = money_format('%.2n', $order->getShippingInclTax());
                } 

                if ($order->getPaymentFeeAmount() > 0) {
                    $order_info['paymentfee'] = money_format('%.2n', $order->getPaymentFeeAmount());
                }
                //UPDATE MICROCASH
                $this->_mcApi->ConfirmOrder($order_info);
            }
        }
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Microcash_Twiga::confirm');
    }

}

